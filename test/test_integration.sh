#!/bin/bash

set -e -x

TEST_BASE="$(dirname $(readlink -f ${0}))"
PROJECT_BASE="$(readlink -f ${TEST_BASE}/..)"

cd "${PROJECT_BASE}"

MP_VERSION=$1

if test -d test/sandbox-environment-${MP_VERSION}
then
    SANDBOX=test/sandbox-environment-${MP_VERSION}
else
    SANDBOX=test/sandbox-environment
fi

MP_DIST_FOLDER=midpoint-${MP_VERSION}
MP_DIST_ARCHIVE=midpoint-${MP_VERSION}-dist.tar.gz

# Delete potentially already existing distribution folder
rm -rf ${MP_DIST_FOLDER}

# Download distribution if missing
test -f ${MP_DIST_ARCHIVE} || wget --progress=dot:giga https://evolveum.com/downloads/midpoint/${MP_VERSION}/${MP_DIST_ARCHIVE}

# Extract distribution
tar xf ${MP_DIST_ARCHIVE}

export PATH=$PATH:$PWD/${MP_DIST_FOLDER}/bin
midpoint.sh start

cp ${SANDBOX}/original-repository1.csv /tmp/repository1.csv
cp ${SANDBOX}/original-repository2.csv /tmp/repository2.csv
echo 'assert len(client.get_users()) == 1' | midpoint-cli script run -
midpoint-cli user ls
midpoint-cli put ${SANDBOX}/object-template-user.xml
midpoint-cli put ${SANDBOX}/resource-repository1.xml
midpoint-cli resource ls
midpoint-cli resource test \"Repository 1\"
midpoint-cli put ${SANDBOX}/resource-repository1-Reconcile.xml
midpoint-cli task ls
midpoint-cli task run \"R1 Reconcile\"
midpoint-cli user ls
echo 'assert len(client.get_users()) == 3' | midpoint-cli script run -
echo "assert 'Hannibal Smith' in [u['FullName'] for u in client.get_users()]" | midpoint-cli script run -
echo "assert 'hannibal.smith@mercernary4hire.com' in [u['Email'] for u in client.get_users()]" | midpoint-cli script run -
midpoint.sh stop
